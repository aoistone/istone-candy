/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
ACC.productDetail = {


	initPageEvents: function ()
	{


		$('.productImageGallery .jcarousel-skin').jcarousel({
			vertical: true
		});


		$(document).on("click","#imageLink, .productImageZoomLink",function(e){
			e.preventDefault();

			$.colorbox({
				href:$(this).attr("href"),
				height:555,
				onComplete: function() {
				    ACC.common.refreshScreenReaderBuffer();

					$('#colorbox .productImageGallery .jcarousel-skin').jcarousel({
						vertical: true
					});

				},
				onClosed: function() {
					ACC.common.refreshScreenReaderBuffer();
				}
			});
		});



		$(".productImageGallery img").click(function(e) {
			$(".productImagePrimary img").attr("src", $(this).attr("data-primaryimagesrc"));
			$("#zoomLink, #imageLink").attr("href",$("#zoomLink").attr("data-href")+ "?galleryPosition="+$(this).attr("data-galleryposition"));
			$(".productImageGallery .thumb").removeClass("active");
			$(this).parent(".thumb").addClass("active");
		});


		$(document).on("click","#colorbox .productImageGallery img",function(e) {
			$("#colorbox  .productImagePrimary img").attr("src", $(this).attr("data-zoomurl"));
			$("#colorbox .productImageGallery .thumb").removeClass("active");
			$(this).parent(".thumb").addClass("active");
		});



		$("body").on("keyup", "input[name=qtyInput]", function(event) {
  			var input = $(event.target);
		  	var value = input.val();
		  	var qty_css = 'input[name=qty]';
  			while(input.parent()[0] != document) {
 				input = input.parent();
 				if(input.find(qty_css).length > 0) {
  					input.find(qty_css).val(value);
  					return;
 				}
  			}
		});




		$("#Size").change(function () {
			var url = "";
			var selectedIndex = 0;
			$("#Size option:selected").each(function () {
				url = $(this).attr('value');
				selectedIndex = $(this).attr("index");
			});
			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});

		$("#variant").change(function () {
			var url = "";
			var selectedIndex = 0;

			$("#variant option:selected").each(function () {
				url = $(this).attr('value');
				selectedIndex = $(this).attr("index");
			});
			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});

		$(".selectPriority").change(function () {
			var url = "";
			var selectedIndex = 0;

			url = $(this).attr('value');
			selectedIndex = $(this).attr("index");

			if (selectedIndex != 0) {
				window.location.href=url;
			}
		});
	},
	throwCandy : function(qty) {
		var $parent = $(".productImagePrimaryLink");
		$parent.css({
			position: "relative",
		});
		var $cart = $(".count");
		var $imgToClone =$(".productImagePrimaryLink img")
		$imgToClone.clone()
			.offset({
				top: $imgToClone.offset().top,
				left: $imgToClone.offset().left
			})
			.css({
				'opacity': '0.8',
				'position': 'absolute',
				'height':'300px',
				'width':'300px'
			})
			.appendTo($('body'))
			.animate({
				'top': $cart.offset().top,
				'left': $cart.offset().left,
				'width': 20,
				'border-radius': 20,
				'height': 20
			}, 600, 'easeOutSine', function(){
				ACC.productDetail.addToCart(qty);
				$(this).remove()
			});



	},
	throwCandyFromList : function(qty, index) {
		var $parent = $(".productImagePrimaryLink");
		$parent.css({
			position: "relative",
		});
		var $cart = $(".count");

		var $imgToClone =$($('input[name=productCode]')[index]).siblings('.thumb');
		$imgToClone.clone()
			.offset({
				top: $imgToClone.offset().top,
				left: $imgToClone.offset().left
			})
			.css({
				'opacity': '0.8',
				'position': 'absolute',
			})
			.appendTo($('body'))
			.animate({
				'top': $cart.offset().top,
				'left': $cart.offset().left,
				'width': 20,
				'border-radius': 20,
				'height': 20
			}, 600, 'easeOutSine', function(){
				ACC.productDetail.addToCart(qty, index);
				$(this).remove()
			});



	},
	poll : function () {

		var stock = $(".stock_message");
		var numbers;
		var text;
		if (stock != null && stock.text().match('[0-9]+') != null) {

			numbers = stock.text().match('[0-9]+')[0];
			text = stock.text().match('[^\-0-9]+')[0];
		}
		var productCode = ACC.config.currentProductCode;
		if (productCode) {
			$.ajax({
				url: ACC.config.webserviceurl + "/candy/v2/b2ctelco/products/" + productCode + "/naivestock"
			}).done(function (result) {
				// only update if stocklevel is > 0 (since it's only displayed then)
				if (result.stockLevel != numbers && result.stockLevel > 0) {
					stock.text(result.stockLevel + text);
					ACC.productDetail.throwCandy(numbers - result.stockLevel);

				} else if (result.stockLevel < 0) {
					stock.text("" +
						"");
				}
				setTimeout(ACC.productDetail.poll, ACC.config.pollingTimout);
			});
		}
	},
	addToCart : function(qty, index){
		if(index == undefined) {
			var form = $("#addToCartForm");
			form.find(".qty").val(qty)
			$(".addToCartButton").click();
		}else{
			var form = $('.add_to_cart_form')[index]
			var qtyInput = $(form).find(".qty");
			qtyInput.val(qty);
			$(form).find(".addToCartButton").click();
		}
	},
	pollList : function () {
		var stockMessages = $(".stock_message");
		stockMessages.each(function(index){
			var oldStock = $(stockMessages[index]).text();
			var productCode = $('input[name=productCode]')[index].value;
			if(!oldStock){
				$.ajax({
					url: ACC.config.webserviceurl + "/candy/v2/b2ctelco/products/" + productCode + "/naivestock"
				}).done(function (result) {
					$(stockMessages[index]).text(result.stockLevel);
				});
			}else{
				$.ajax({
					url: ACC.config.webserviceurl + "/candy/v2/b2ctelco/products/" + productCode + "/naivestock"
				}).done(function (result) {
					if (result.stockLevel != oldStock && result.stockLevel > 0) {
						$(stockMessages[index]).text(result.stockLevel);
						ACC.productDetail.throwCandyFromList(oldStock - result.stockLevel, index);
					}
				});
			}

		})
			setTimeout(ACC.productDetail.pollList, ACC.config.pollingTimout);
	}
};

$(document).ready(function ()
{

	with(ACC.productDetail)
	{
		initPageEvents();
		var isProductList = $('.pageType-CategoryPage');

		if ($(isProductList).length) {
			pollList()
		} else {
			poll();
		}
	}
});

