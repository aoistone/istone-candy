# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
# All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
#
# Import the CMS Site configuration for the store
#

# ImpEx for Importing Products into B2C Telco Store

# Macros / Replacement Parameter definitions
$productCatalog=b2ctelcoProductCatalog
$productCatalogName=B2C Telco Product Catalog
$catalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]
$picture=picture(code, $catalogVersion)


INSERT_UPDATE CMSSite;uid[unique=true];urlPatterns[mode=append];name[lang=en]
;b2ctelco;(?i)^https?://candystore.istone\.[^/]+(|/.*|\?.*)$;Istone Candystore

INSERT_UPDATE Category;code[unique=true];allowedPrincipals(uid)[default='customergroup'];$catalogVersion;name[lang=en]
;candy;;;Candy;

#INSERT_UPDATE Category;code[unique=true];name[lang=en]
#;1;

$supercategories=source(code, $catalogVersion)[unique=true]
$categories=target(code, $catalogVersion)[unique=true]
INSERT_UPDATE CategoryCategoryRelation;$categories;$supercategories
;candy;1

$supercategories=supercategories(code, $catalogVersion)
INSERT_UPDATE Accessory;code[unique=true];$supercategories;manufacturerName;manufacturerAID;description[lang=en];name[lang=en];unit(code)[default=pieces];ean;variantType(code);$catalogVersion
;0000001;candy,1;Cloetta;clo002;godisbit1;godisbit1;;;
;0000002;candy,1;Cloetta;clo002;godisbit2;godisbit2;;;
;0000003;candy,1;Cloetta;clo003;godisbit3;godisbit3;;;

$thumbnail=thumbnail(code, $catalogVersion)
$picture=picture(code, $catalogVersion)
$thumbnails=thumbnails(code, $catalogVersion)
$detail=detail(code, $catalogVersion)
$normal=normal(code, $catalogVersion)
$others=others(code, $catalogVersion)
$data_sheet=data_sheet(code, $catalogVersion)
$medias=medias(code, $catalogVersion)
$galleryImages=galleryImages(qualifier, $catalogVersion)
UPDATE Product;code[unique=true];$picture;$thumbnail;$detail;$normal;$thumbnails;$others;$galleryImages;$catalogVersion
;0000001;/300Wx300H/1229144-8866.jpg;/96Wx96H/1229144-8866.jpg;/1200Wx1200H/1229144-8866.jpg;/300Wx300H/1229144-8866.jpg;/96Wx96H/1229144-8866.jpg;/515Wx515H/1229144-8866.jpg,/300Wx300H/1229144-8866.jpg,/96Wx96H/1229144-8866.jpg,/65Wx65H/1229144-8866.jpg,/30Wx30H/1229144-8866.jpg;1229144-8866
;0000002;/300Wx300H/1279473-1166.jpg;/96Wx96H/1279473-1166.jpg;/1200Wx1200H/1279473-1166.jpg;/300Wx300H/1279473-1166.jpg;/96Wx96H/1279473-1166.jpg;/515Wx515H/1279473-1166.jpg,/300Wx300H/1279473-1166.jpg,/96Wx96H/1279473-1166.jpg,/65Wx65H/1279473-1166.jpg,/30Wx30H/1279473-1166.jpg;1279473-1166
;0000003;/300Wx300H/1232742-9580.jpg;/96Wx96H/1232742-9580.jpg;/1200Wx1200H/1232742-9580.jpg;/300Wx300H/1232742-9580.jpg,/300Wx300H/1232742_1617.jpg,/300Wx300H/1232742_326.jpg,/300Wx300H/1232742_5707.jpg,/300Wx300H/1232742_720.jpg;/96Wx96H/1232742-9580.jpg,/96Wx96H/1232742_1617.jpg,/96Wx96H/1232742_326.jpg,/96Wx96H/1232742_5707.jpg,/96Wx96H/1232742_720.jpg;/515Wx515H/1232742-9580.jpg,/300Wx300H/1232742-9580.jpg,/96Wx96H/1232742-9580.jpg,/65Wx65H/1232742-9580.jpg,/30Wx30H/1232742-9580.jpg,/515Wx515H/1232742_1617.jpg,/300Wx300H/1232742_1617.jpg,/96Wx96H/1232742_1617.jpg,/65Wx65H/1232742_1617.jpg,/30Wx30H/1232742_1617.jpg,/515Wx515H/1232742_326.jpg,/300Wx300H/1232742_326.jpg,/96Wx96H/1232742_326.jpg,/65Wx65H/1232742_326.jpg,/30Wx30H/1232742_326.jpg,/515Wx515H/1232742_5707.jpg,/300Wx300H/1232742_5707.jpg,/96Wx96H/1232742_5707.jpg,/65Wx65H/1232742_5707.jpg,/30Wx30H/1232742_5707.jpg,/515Wx515H/1232742_720.jpg,/300Wx300H/1232742_720.jpg,/96Wx96H/1232742_720.jpg,/65Wx65H/1232742_720.jpg,/30Wx30H/1232742_720.jpg;1232742-9580,1232742_1617,1232742_326,1232742_5707,1232742_720

$prices=Europe1prices[translator=de.hybris.platform.europe1.jalo.impex.Europe1PricesTranslator]
$europe1pricefactory_ptg=Europe1PriceFactory_PTG(code)[default=us-sales-tax-full]

# Set product approval status to Approved only for those products that have prices.
$approved=approvalstatus(code)[default='approved']
$billingEvent=billingEvent(code)[default='onfirstbill']

UPDATE Accessory;code[unique=true];$prices;$approved;$catalogVersion;$europe1pricefactory_ptg
;0000001;1 USD
;0000002;2 USD
;0000003;3 USD

$vendor=b2ctel

INSERT_UPDATE StockLevel;productCode[unique=true];available[default=3000];warehouse(code)[unique=true,default=warehouse_telco];inStockStatus(code)[default=notSpecified];maxPreOrder[default=0];maxStockLevelHistoryCount[default=-1];overSelling[default=0];preOrder[default=0];reserved[default=0]
;0000001
;0000002
;0000003

INSERT_UPDATE Product;code[unique=true];stockLevels(productCode,warehouse(code));vendors(code)[default=$vendor];$catalogVersion
;0000001;0000001:warehouse_telco
;0000002;0000002:warehouse_telco
;0000003;0000003:warehouse_telco

$contentCatalog=b2ctelcoContentCatalog
$contentCatalogName=B2C Telco Content Catalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$productCV=catalogVersion(catalog(id[default=$productCatalog]),version[default='Staged'])[unique=true,default=$productCatalog:Staged]

INSERT_UPDATE CMSLinkComponent;$contentCV[unique=true];uid[unique=true];name;url;&linkRef;&componentRef;target(code)[default='sameWindow'];category(code, $productCV);linkName[lang=en]
;;CandyCategoryLink;Candy Category Link;/Open-Catalogue//c/candy;CandyCategoryLink;CandyCategoryLink;;candy;Candy

INSERT_UPDATE CMSNavigationNode;uid[unique=true];$contentCV[unique=true];name;parent(uid, $contentCV);links(&linkRef);&nodeRef;title
;CandyNavNode;;Candy Category;B2cTelcoNavNode;CandyCategoryLink;CandyNavNode;Candy

INSERT_UPDATE NavigationBarComponent;$contentCV[unique=true];uid[unique=true];name;wrapAfter;link(uid, $contentCV);styleClass;navigationNode(&nodeRef)
;;CandyBarComponent;Candy Bar Component;10;CandyCategoryLink;;CandyNavNode

INSERT_UPDATE NavigationBarCollectionComponent;$contentCV[unique=true];uid[unique=true];name;components(uid, $contentCV)[mode=append];
;;NavBarComponent;Navigation Bar Collection Component;CandyBarComponent;

#behövs detta för att priserna ska visas?
#UPDATE USER; uid[unique=true];sessionLanguage(isocode);sessionCurrency(isocode)
#;anonymous;en;USD;